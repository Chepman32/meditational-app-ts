import React, {useEffect, useState} from 'react';
import {View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import track1 from "../../assets/audio/alpha-space-meditation-128640(1).mp3"
import track2 from "../../assets/audio/bad-mood-21193(1).mp3"
import track3 from "../../assets/audio/deep-bass-guitar-beats-offical-a.mp3"
import track4 from "../../assets/audio/growing-up-123251(1).mp3"
import track5 from "../../assets/audio/his-touch-long-13135.mp3"
import track6 from "../../assets/audio/mindfulness-relaxation-amp-medit.mp3"
import track7 from "../../assets/audio/mindfulness-relaxation-amp-meditation-music-22174.mp3"
import track8 from "../../assets/audio/morning-garden-acoustic-chill-15.mp3"
import track9 from "../../assets/audio/morning-garden-acoustic-chill-15(1).mp3"
import track10 from "../../assets/audio/morning-garden-acoustic-chill-15013.mp3"
import track11 from "../../assets/audio/muladara-chakra-music-138093(1).mp3"
import track12 from "../../assets/audio/october-8902.mp3"
import track13 from "../../assets/audio/peaceful-meditation-124457.mp3"
import track14 from "../../assets/audio/relax-music-7738(1).mp3"
import track15 from "../../assets/audio/relaxed-vlog-131746.mp3"
import track16 from "../../assets/audio/relaxing-audio-for-yoga-131673(1).mp3"
import track17 from "../../assets/audio/romance-me-113047(1).mp3"
import track18 from "../../assets/audio/sky-13816(1).mp3"
import track19 from "../../assets/audio/sunday-20797(1).mp3"
const tracks = [
  track1,
  track2,
  track3,
  track4,
  track5,
  track6,
  track7,
  track8,
  track9,
  track10,
  track11,
  track12,
  track13,
  track14,
  track15,
  track16,
  track17,
  track18,
  track19,
]
function getRandomInt(min: number, max: number) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
const index = getRandomInt(0, tracks.length - 1)

var Sound = require('react-native-sound');

var ding = new Sound(tracks[index], error => {
  if (error) {
    console.log('failed to load the sound', error);
    return;
  }
  // if loaded successfully
  console.log(
    'duration in seconds: ' +
      ding.getDuration() +
      'number of channels: ' +
      ding.getNumberOfChannels(),
  );
});
Sound.setCategory('Playback');

var audio = new Sound(
  tracks[index],
  null,
  (error: string) => {
    if (error) {
      console.log('failed to load the sound', error);
      return;
    }
    // if loaded successfully
    console.log(
      'duration in seconds: ' +
        audio.getDuration() +
        'number of channels: ' +
        audio.getNumberOfChannels(),
    );
  },
);
export const BigPlayer = () => {
  const [playing, setPlaying] = useState<boolean>(false);
  useEffect(() => {
    ding.setNumberOfLoops(-1)
    playPause()
    ding.setVolume(1);
    return () => {
      ding.release();
    };
  }, []);
  const playPause = () => {
    setPlaying(!playing)
    playing ?
    ding.play(success => {
      if (success) {
        console.log('successfully finished playing');
      } else {
        console.log('playback failed due to audio decoding errors');
      }
    })
    :
    ding.pause()
  };
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={playPause}>
        <Text style={styles.playBtn}>
          {
            !playing ? "Pause" : "Play"
          }
        </Text>
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  playBtn: {
    padding: 20,
    fontSize: 30,
    fontWeight: "500",
    backgroundColor: "rgba(218, 220, 224, 0.65)",
    overflow: "hidden",
    borderRadius: 10,
  },
});
