import { View, StyleSheet, ScrollView } from 'react-native'
import React, { useState } from 'react'
import { BigPlayer } from './BigPlayer'

export default function BigHomescreen() {
  return (
    <View style={styles.container} >
        <BigPlayer/>
      </View>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  }
})